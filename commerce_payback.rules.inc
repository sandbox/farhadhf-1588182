<?php

/**
 * @file
 * Rules actions and events for commerce_payback module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_payback_rules_action_info() {
  $actions = array(
    'commerce_payback_create_payback' => array(
      'label' => t('Create payback order for the line item'),
      'group' => t('Commerce payback'),
      'parameter' => array(
        'line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line Item'),
        ),
        'marketplace_fee' => array(
          'type' => 'decimal',
          'label' => t('Marketplace fee percent of the order amount'),
        ),
      ),
      'provides' => array(
        'payback_order' => array(
          'type' => 'commerce_order',
          'label' => t('Payback order'),
        ),
      ),
    ),
  );
  
  return $actions;
}

/**
 * Implements hook_rules_event_info().
 */
function commerce_payback_rules_event_info() {
  $events = array();
  
  $events['commerce_payback_buyer_confirm'] = array(
    'label' => t('Buyer confirmed receiving the product'),
    'group' => t('Commerce payback'),
    'variables' => array(
      'line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item'),
      ),
      'buyer_user' => array(
        'type' => 'user',
        'label' => t('Buyer'),
      ),
      'seller_user' => array(
        'type' => 'user',
        'label' => t('Seller'),
      ),
    ),
  );
  return $events;
}

function commerce_payback_create_payback($line_item, $fee) {
  $buy_order = commerce_order_load($line_item->order_id);
  $buyer = user_load($buy_order->uid);
  $product_reference = field_get_items('commerce_line_item', $line_item, 'commerce_product');
  if (!$product_reference) {
    watchdog('commerce_payback', t('error in create payback action, there is no product reference on the line item!'), array(), WATCHDOG_ERROR);
    return FALSE;
  }
  $bought_product = commerce_product_load($product_reference[0]['product_id']);
  $seller = user_load($bought_product->uid);
  $product = commerce_product_new('commerce_payback');
  
  // calculate payback amount
  $amount = $line_item->commerce_total[LANGUAGE_NONE][0]['amount'];
  $currency_code = $line_item->commerce_total[LANGUAGE_NONE][0]['currency_code'];
  $full_price = round(commerce_currency_amount_to_decimal($amount, $currency_code), 2);
  $percent = 1.0 - $fee;
  $end_price = round($full_price * $percent, 2);
  
  $product->commerce_price[LANGUAGE_NONE][0]['amount'] = $end_price;
  $product->commerce_price[LANGUAGE_NONE][0]['currency_code'] = $currency_code;
  $product->uid = 0;
  $product->type = 'payback';
  $product->sku = $seller->uid . '_' . $line_item->line_item_id;
  $product->title = t('Payback for @product bought by @user', array('@product' => $bought_product->title, '@user' => $buyer->name));
  commerce_product_save($product);

  $order = commerce_payback_find($seller->uid);
  commerce_order_save($order);
  // create a new line item referencing $product and add it to $order.
  $new_line_item = commerce_product_line_item_new($product, 1, $order->order_id, array('originating_line_item_id' => $line_item->line_item_id), 'commerce_payback_line_item');
  commerce_line_item_save($new_line_item);

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $wrapper->commerce_line_items[] = $new_line_item;
  $wrapper->save();

  return array(
    'payback_order' => $order
  );
}

