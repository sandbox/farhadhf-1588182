<?php

function commerce_payback_bought_orders($user) {
  // TODO: Add sort ability to this table 
  $header = array(
    'order_id' => t('Commerce order'),
    'product' => t('Product'),
    'quantity' => t('Quantity'),
    'total' => t('Total'),
    'created' => t('Created'),
    'updated' => t('Updated'),
    'status' => t('Status'),
  );

  drupal_set_title(t('Buy orders'));
  $data = array();
  $orders = db_select('commerce_order', 'co')->fields('co', array('order_id'))->condition('type', 'commerce_payback', '!=')->condition('uid', $user->uid, '=')->execute();
  
  foreach($orders as $order_id) {
    $order = commerce_order_load($order_id->order_id);
    if (!in_array($order->status, array('pending', 'processing', 'completed'))) {
      continue;
    }
    foreach($order->commerce_line_items['und'] as $delta => $lineitem) {
      if ($line_item = commerce_line_item_load($lineitem['line_item_id'])) {
        if ($line_item->type == 'product' || $line_item->type == 'commerce_auction_lineitem') {
          // load $product and then populate $data for this row.
          $product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
          
          // load status from status field attached to the line item
          $status = t('Undefined');
          $status_field = field_get_items('commerce_line_item', $line_item, 'line_item_state');
          
          if ($status_field[0]['value'] == 'processing') {
            $seller = user_load($product->uid);
            
            $status = t('Sent by !seller', array('!seller' => l($seller->name, 'user/' . $seller->uid))) . ' (' . l(t('Edit'), 'user/'. $user->uid . '/orders/bought/' . $order->order_id . '/' . $line_item->line_item_id . '/edit', array('query' => array('destination' => '/' . request_path()))) . ')';
          }
          else if ($status_field[0]['value'] == 'completed'){
            $status = t('Received by you (Completed)');
          }
          else {
            $status =  t('Not sent');
          }
          
          $currency = commerce_currency_load(commerce_default_currency());
          
          $data[$line_item->line_item_id] = array(
            'order_id' => l($order->order_id, 'user/'. $user->uid . '/orders/bought/' . $order->order_id),
            'product' => l($product->title, $line_item->commerce_display_path['und'][0]['value']),
            'quantity' => $line_item->quantity,
            'total' => $line_item->commerce_total['und'][0]['amount'] . ' ' . $currency['symbol'],
            'created' => format_date($order->created),
            'updated' => format_date($order->changed),
            'status' => $status ,
          );
        }
      }
    }
  }
  
  return theme('table', array('header' => $header, 'rows' => $data, 'empty' => t('No orders yet!')));
}

function commerce_payback_sold_orders($account) {
  $header = array(
    'order_id' => t('Commerce Order'),
    'product' => t('Product'),
    'quantity' => t('Quantity'),
    'total' => t('Total'),
    'created' => t('Created'),
    'updated' => t('Updated'),
    'status' => t('Status'),
  );
  
  drupal_set_title(t('Sell orders'));
  
  // load line items that contain products owned by $user
  $query = db_select('commerce_product', 'cp');
  $query->condition('cp.uid', $account->uid);
  $query->innerJoin('field_data_commerce_product', 'productfield', 'productfield.commerce_product_product_id = cp.product_id');
  $query->innerJoin('commerce_line_item', 'cli', 'cli.line_item_id = productfield.entity_id AND cli.type IN (:type1, :type2)', array(':type1' => 'product', ':type2' => 'commerce_auction_lineitem'));
  $query->innerJoin('commerce_order', 'co', 'co.order_id = cli.order_id AND co.status IN (:status1, :status2, :status3)', array(':status1' => 'pending', ':status2' => 'processing', ':status3' => 'completed'));
  $res = $query->fields('cli', array('order_id'))->fields('cli', array('line_item_id'))->fields('cp', array('product_id'))->execute();
  
  $data = array();
  foreach ($res as $result) {
    $product = commerce_product_load($result->product_id);
    $order = commerce_order_load($result->order_id);
    $line_item = commerce_line_item_load($result->line_item_id);
    $currency = commerce_currency_load(commerce_default_currency());
    
    $status = t('Undefined');
    $status_field = field_get_items('commerce_line_item', $line_item, 'line_item_state');
    
    if ($status_field[0]['value'] == 'processing') {
      $status =  t('Sent by you') . ' (' . l(t('Edit'), 'user/'. $account->uid . '/orders/sold/' . $order->order_id . '/' . $line_item->line_item_id . '/edit', array('query' => array('destination' => '/' . request_path()))) . ')';
    }
    else if ($status_field[0]['value'] == 'completed'){
      // TODO: make buyer link to buyers profile, allow the seller to rate him.
      $buyer = user_load($order->uid);
      $status = t('Received by !buyer (Completed)', array('!buyer' => l($buyer->name, 'user/' . $buyer->uid)));
    }
    else {
      $status =  t('Not sent') . ' (' . l(t('Edit'), 'user/'. $account->uid . '/orders/sold/' . $order->order_id . '/' . $line_item->line_item_id . '/edit', array('query' => array('destination' => '/' . request_path()))) . ')';
    }
    
    $data[$line_item->line_item_id] = array(
      'order_id' => l($order->order_id, 'user/'. $account->uid . '/orders/sold/' . $order->order_id . '/' . $line_item->line_item_id),
      'product' => l($product->title, $line_item->commerce_display_path['und'][0]['value']),
      'quantity' => $line_item->quantity,
      'total' => $line_item->commerce_total['und'][0]['amount'] . ' ' . $currency['symbol'],
      'created' => format_date($order->created),
      'updated' => format_date($order->changed),
      'status' => $status ,
    );
  }
  
  return theme('table', array('header' => $header, 'rows' => $data, 'empty' => t('No orders yet!')));
}

function commerce_payback_edit_order_status($form, &$form_state, $op, $order, $line_item) {
  $warning = ($op == 'bought') ? t('Please check this option only if you really have received the product as described in the by the seller.<br />If you check this option, we will pay the seller. This means that you have accepted what the sellre has sent for you.<br />You won\'t be able to change this option after you have set it.'):
                                 t('Please check this option only if you really have sent to product to the buyer.');
  global $user;
  if ($op == 'bought') {
    if (!isset($line_item->line_item_state['und'][0]['value']) || $line_item->line_item_state['und'][0]['value'] == 'processing') {
      $form['state'] = array(
        '#type' => 'checkbox',
        '#title' => ($op == 'bought') ? t('I have received this product') : t('I have sent this product'),
        '#description' => $warning
      );
    }
    else {
      $form['state'] = array(
        '#type' => 'checkbox',
        '#title' => t('I have received this product'),
        '#disabled' => TRUE,
        '#value' => TRUE,
      );
    }
  }
  else if ($op == 'sold') {
    if (!isset($line_item->line_item_state['und'][0]['value']) || !$line_item->line_item_state['und'][0]['value']) {
      $form['state'] = array(
        '#type' => 'checkbox',
        '#title' => t('I have sent this product'),
        '#description' => $warning
      );
    }
    else {
      $form['state'] = array(
        '#type' => 'checkbox',
        '#title' => t('I have sent this product'),
        '#disabled' => TRUE,
        '#value' => TRUE,
      );
    }
  }
  field_attach_form('commerce_line_item', $line_item, $form, $form_state);
  $form['commerce_unit_price'] = array();
  $form['commerce_display_path'] = array();
  $form['commerce_total'] = array();
  $form['commerce_product'] = array();
  $form['line_item_state'] = array();
  $form['commerce_unit_price'] = array();
  
  if ($op == 'bought') {
    $form['shipping_info'] = array();
    if (isset($form['post_code']['und'][0]['value']['#default_value']) && $form['post_code']['und'][0]['value']['#default_value'] ) {
      $form['post_code_b'] = array(
        '#type' => 'markup',
        '#markup' => '<p>' . t('Post investigation code: @code', array('@code' => $form['post_code']['und'][0]['value']['#default_value'])) . '</p>',
      );
    }
    $form['post_code'] = array();
    if (isset($form['shipping_method']['und'][0]['value']['#default_value']) && $form['shipping_method']['und'][0]['value']['#default_value']) {
      $form['shipping_method_b'] = array(
        '#type' => 'markup',
        '#markup' => '<p>' . t('Shipping method: @method', array('@method' => $form['shipping_method']['und'][0]['value']['#default_value'])) . '</p>',
      );
    }
    $form['shipping_method'] = array();
  }
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save!'),
    '#weight' => 20,
  );
  
  $form_state['#op'] = $op;
  $form_state['#line_item'] = $line_item;
  $form_state['#order'] = $order;
  return $form;
}

function commerce_payback_edit_order_status_validate($form, &$form_state) {
  if (isset($form_state['values']['shipping_info']['und'][0]['upload']) && $form_state['values']['shipping_info']['und'][0]['upload']) {
    field_attach_form_validate('commerce_line_item', $form_state['#line_item'], $form, $form_state);
  }
  else {
    if (isset($form_state['values']['post_code']['und'][0]['value']) &&
        $form_state['values']['post_code']['und'][0]['value'] &&
        !is_numeric($form_state['values']['post_code']['und'][0]['value'])) {
      form_set_error($form['post_code'], t('Post code should be a numeric value'));
    }
  }
}

function commerce_payback_edit_order_status_submit($form, &$form_state) {
  if (empty($form_state['values']['state'])) {
    return;
  }
  $order = $form_state['#order'];
  $buyer = user_load($order->uid);
  $line_item = $form_state['#line_item'];
  $op = $form_state['#op'];

  switch($op) {
    case 'sold':
      // update line_item state to sent(processing);
      $line_item->line_item_state['und'][0]['value'] = 'processing';
      commerce_line_item_save($line_item);
      break;
    
    case 'bought':
      // update line_item state to received(completed);
      $line_item->line_item_state['und'][0]['value'] = 'completed';
      commerce_line_item_save($line_item);
      // fire a Rules event.
      // Create a Rules action that creates the payback order, etc...
      $bought_product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      $seller = user_load($bought_product->uid);
      rules_invoke_event('commerce_payback_buyer_confirm', $line_item, $buyer, $seller);
      break;
    
    default:
      break;
  }
  
  // check for line_item states, if all of the product/auction line items are completed, update order state
  $completed = 1;
  foreach($order->commerce_line_items['und'] as $delta => $line_item_id) {
    if ($li = commerce_line_item_load($line_item_id['line_item_id'])) {
      if ($li->type == 'commerce_auction_lineitem' || $li->type == 'product') {
        if (isset($li->line_item_state) && $li->line_item_state['und'][0]['value'] != 'completed') {
          $completed = 0;
          break;
        }
      }
    }
  }
  if ($completed) {
    commerce_order_status_update($order, 'completed');
  }
  field_attach_submit('commerce_line_item', $line_item, $form, $form_state);
  commerce_line_item_save($line_item);
}


function commerce_payback_sell_order_view($user, $order, $line_item) {
  // show the $line_item info, and order shipment / payment addressess
  $header = array(
    'title' => t('Title'),
    'unit_price' => t('Unit price'),
    'quantity' => t('Quantity'),
    'status' => t('Status'),
    'total' => t('Total'),
  );
  
  $product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
  $currency = commerce_currency_load(commerce_default_currency());
  $state = t('Not sent');
  
  if (isset($line_item->line_item_state['und'][0]['value'])) {
    if ($line_item->line_item_state['und'][0]['value'] == 'processing') {
      $state = t('Sent');
    }
    else {
      $state = t('Received');
    }
  }
  if ($state != 'Received') {
    $state .= ' (' . l(t('Edit'), 'user/' . $user->uid . '/orders/bought/' . $order->order_id . '/' . $line_item->line_item_id . '/edit') . ')';
  }
  
  $data = array();
  $data[] = array(
    'title' => $product->title,
    'unit_price' => $product->commerce_price['und'][0]['amount'] / 100 . ' ' . $currency['symbol'],
    'quantity' => $line_item->quantity,
    'status' => $state,
    'total' => $line_item->commerce_total['und'][0]['amount'] / 100 . ' ' . $currency['symbol'],
  );
  
  $ret = theme('table', array('header' => $header, 'rows' => $data, 'empty' => t('No orders yet!'), ));
  $ret .= '<h3>' . t('Shipment address') . '</h3>';
  $profile = commerce_customer_profile_load($order->commerce_customer_billing['und'][0]['profile_id']);
  $output = entity_view('commerce_customer_profile', array($profile->profile_id => $profile), 'full', NULL, TRUE);
  $ret .= render($output['commerce_customer_profile'][$profile->profile_id]);
  return $ret;
}

function commerce_payback_buy_order_view($user, $order) {
  $header = array(
    'title' => t('Title'),
    'unit_price' => t('Unit price'),
    'quantity' => t('Quantity'),
    'status' => t('Status'),
    'total' => t('Total'),
  );
  
  foreach ($order->commerce_line_items['und'] as $line_item_id) {
    $line_item = commerce_line_item_load($line_item_id['line_item_id']);
    if ($line_item->type == 'commerce_auction_lineitem' || $line_item->type == 'product') {
      $product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
      $state = t('Not sent');
      if (isset($line_item->line_item_state['und'][0]['value'])) {
        if ($line_item->line_item_state['und'][0]['value'] == 'processing') {
          $state = t('Sent');
        }
        else {
          $state = t('Received');
        }
      }
      $currency = commerce_currency_load(commerce_default_currency());
      $data[$line_item_id['line_item_id']] = array(
        'title' => $product->title,
        'unit_price' => $product->commerce_price['und'][0]['amount'] / 100 . ' ' . $currency['symbol'],
        'quantity' => $line_item->quantity,
        'status' => $state,
        'total' => $line_item->commerce_total['und'][0]['amount'] / 100 . ' ' . $currency['symbol'],
      );
    }
  }
  
  $ret = theme('table', array('header' => $header, 'rows' => $data, 'empty' => t('No orders yet!')));
  $ret .= '<p>' . t('Order total') . ': ' . $order->commerce_order_total['und'][0]['amount'] / 100 . ' ' . $currency['symbol'] . '</p>';
  return $ret;
}
