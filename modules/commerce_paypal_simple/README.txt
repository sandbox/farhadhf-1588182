---README---
This module is used to transfer money using PayPal Simple Adaptive Payment API.
The payment method should not be used with the regular product order payments, It is just a place holder.
You can use the Rules action "Transfer payback order total to seller paypal account" in a drupal commerce
marketplace to transfer money from sender account (defined as rules action parameter) to the seller paypal
account (field_paypal_email on user entity).

---INSTALL---
The module depends on PayPal Adaptive Payments NVP SDK.
Download it from https://www.x.com/sites/default/files/PayPal_Platform_PHP_Nvp_SDK_0.zip
extract it and copy php_nvp_sdk/Lib/ to sites/all/libraries/paypal .
If you do this right, You'll find CallerService.php in sites/all/libraries/paypal/

Now open sites/all/libraries/paypal/Config/Config.php and change values of
API_USERNAME
API_PASSWORD
API_SIGNATURE
API_ENDPOINT (this is the server address, default value is sandbox, live server is https://svcs.paypal.com/)
APPLICATION_ID (more info at http://www.wp-instantpay.com/application-id/ - no need to change this for sandbox server)
DEVICE_IPADDRESS (no need to change for sandbox usage)

and save the file.
