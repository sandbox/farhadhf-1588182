<?php

/**
 * @file
 * Rules actions and events for commerce_payback module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_paypal_simple_rules_action_info() {
  $actions = array(
    'commerce_paypal_simple_transfer_money' => array(
      'label' => t('Transfer payback order total to seller paypal account'),
      'group' => t('Commerce Paypal simple'),
      'parameter' => array(
        'payback_order' => array(
          'type' => 'commerce_order',
          'label' => t('Payback order'),
        ),
        'sender_email' => array(
          'type' => 'text',
          'label' => t('Sender Paypal Email'),
        ),
      ),
    ),
  );
  
  return $actions;
}

function commerce_paypal_simple_transfer_money($order, $sender_email) {
  require_once libraries_get_path('paypal') . '/CallerService.php';
  require_once "Constants.php";

  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = $wrapper->commerce_order_total->amount->value();

  // Return if no Paypal field in user.
  $account = user_load($order->uid);
  $receiver_paypal_email_field = field_get_items('user', $account, 'field_paypal_email');
  if (!isset($receiver_paypal_email_field[0]['value'])) {
    watchdog('commerce_paypal_simple', 'No paypal in seller.');
    drupal_set_message(t('User @name has no Paypal account defined.', array('@name' => format_username($account))), 'error');
    return FALSE;
  }
  $receiverEmail = $receiver_paypal_email_field[0]['value'];

  $serverName = $_SERVER['SERVER_NAME'];
  $serverPort = $_SERVER['SERVER_PORT'];
  $currency_code = in_array($currency_code, array_keys(commerce_paypal_wps_currencies())) ? $currency_code : $settings['currency_code'];
  $request_array= array(
    Pay::$actionType => 'PAY',
    Pay::$cancelUrl  => url(request_path(), array('absolute' => TRUE)),
    Pay::$returnUrl => url(request_path(), array('absolute' => TRUE)),
    Pay::$currencyCode  => $currency_code,
    Pay::$clientDetails_deviceId  => DEVICE_ID,
    Pay::$clientDetails_ipAddress  => '127.0.0.1',
    Pay::$clientDetails_applicationId => APPLICATION_ID,
    RequestEnvelope::$requestEnvelopeErrorLanguage => 'en_US',
    Pay::$senderEmail => $sender_email,
    Pay::$feesPayer => 'EACHRECEIVER',
    Pay::$receiverEmail[0] => $receiverEmail,
    Pay::$receiverAmount[0] => $amount,
  );

  $nvpStr = http_build_query($request_array, '', '&');
  $resArray = hash_call('AdaptivePayments/Pay', $nvpStr);
  
  // Prepare a transaction object to log the API response.
  $transaction = commerce_payment_transaction_new('commerce_paypal_simple', $order->order_id);
  // @todo: instance_id ?
  $transaction->instance_id = 'commerce_paypal_simple';
  $transaction->amount = $amount;
  $transaction->currency_code = $currency_code;
  $transaction->payload[REQUEST_TIME] = $resArray;

  $message = array();

  if (in_array($resArray['responseEnvelope.ack'], array('Failure', 'FailureWithWarning'))) {
    watchdog('commerce_paypal_simple', 'ERROR with paypal.');
    drupal_set_message(t('There was an error with Paypal'), 'error');
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $message[] = '<b>' . t('PAY - Failure') . '</b>';
    $message[] = $resArray['payErrorList'];
  }
  elseif (in_array($resArray['responseEnvelope.ack'], array('Success', 'SuccessWithWarning'))) {
    if ($resArray['paymentExecStatus'] == 'COMPLETED') {
      if ($resArray['responseEnvelope.ack'] == 'SuccessWithWarning') {
        $message[0] = '<b>' . t('PAY - Success (with warning)') . '</b>';
        $message[] = $resArray['payErrorList'];
      }
      else {
        $message[] = '<b>' . t('PAY - Success') . '</b>';
      }
      $transaction->remote_id = $resArray['payKey'];
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    }
    else {
      watchdog('commerce_paypal_simple', 'ERROR with paypal - Probably you have forgot to change API_USERNAME, API_PASSWORD and API_SIGNATURE in Config.php');
      drupal_set_message(t('There was an error with Paypal, could not confirm payment.'), 'error');
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $message[] = '<b>' . t('PAY - Failure') . '</b>';
      $message[] = t('The payment needs redirecting to paypal website to be confirmed. Please make sure that API username is identical to sender email.');
    }
  }
  // Set the final message.
  $transaction->message = implode('<br />', $message);
  $transaction->remote_status = $resArray['paymentExecStatus'];
  commerce_payment_transaction_save($transaction);
  if ($transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS) {
    commerce_order_status_update($order, 'completed');
  }
}

